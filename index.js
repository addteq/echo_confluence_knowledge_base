'use strict';
module.change_code = 1;
var _ = require('lodash');
var Alexa = require('alexa-app');
var app = new Alexa.app('ConfluenceSearch');
var confluenceHelperObject = require('./confluence');

app.launch(function(req, res) {
    res.say("Hello. Can I search anything for you in the Confluence Knowledge base today ?");
    res.shouldEndSession(false);
});


app.intent('confluenceSearch', {
    'slots': {
        'RTEXT': 'LITERAL'
    },
    'utterances': ['{who|what|how|where} {|is|to|do|I} {|get|find|search|talk} {|is|to|the} {do something|RTEXT}']
},
function(req, res) {

    var searchtext = req.slot('RTEXT');
    var reprompt = 'Sorry , I didnt hear anything';

    if (_.isEmpty(searchtext) ) {
        var prompt = 'I didn\'t hear anything. Please tell me again .' ;
        res.say(prompt).reprompt(reprompt).shouldEndSession(false);
        return true;
    } else {
        var confluenceHelper = new confluenceHelperObject();

        confluenceHelper.GetConfluence(searchtext).then(function(responsebd) {
            if( responsebd.results.length === 1){
                confluenceHelper.GetConfluencePageContent(responsebd.results[0].id).then(function(response) {
                    res.say(confluenceHelper.formatConfluencePageContent(response)).send();
                }).catch(function(err) {
                    var prompt;
                    if (err.statusCode === 404) {
                        console.log(err.message);
                        prompt = 'Sorry The requested page was not found';
                    } else {
                        prompt = 'Sorry , I am not able to reach the API server';
                    }
                    res.say(prompt).send();
                });
            }else{
                res.say(confluenceHelper.formatConfluenceStatus(responsebd.results)).send();
                res.session('results',responsebd.results);
                res.shouldEndSession(false);
            }

        }).catch(function(err) {
            var prompt;
            if (err.statusCode === 404) {
                prompt = 'Sorry Unable to find Confluence URL. Please setup account linking in your alexa app again';
            } else {
                prompt = 'Sorry Unable to find Confluence URL. Please setup account linking in your alexa app again';
            }
            res.say(prompt).send();
        });
        return false;
    }
});


app.intent('searchDetails', {
    'slots': {
        'UInput': 'AMAZON.NUMBER'
    },
    'utterances': ['{|Number} {UInput}']
},
function(req, res) {
    //get the slot
    var IncorrectPrompt = 'Please try to search again';
    var searchResult = req.session('results');

    if ( typeof searchResult == 'undefined' ){
        res.say('Please tell me what to search for ?').send();
        res.shouldEndSession(false);
    }else{
        var reprompt = 'Sorry , I didnt hear anything';
        var searchentry = req.slot('UInput');
        var confluenceHelper = new confluenceHelperObject();

        if (_.isEmpty(searchentry) ) {
            res.say('Please choose a valid option ' + confluenceHelper.formatConfluenceStatus(searchResult)).send();
            res.shouldEndSession(false);
        }else{
            var contentval = searchResult[searchentry-1];

            console.log("content is " + contentval);

            if ( typeof contentval == 'undefined' ){
                res.say('Please choose a valid option ' + confluenceHelper.formatConfluenceStatus(searchResult)).send();
                res.shouldEndSession(false);
            }else{
                confluenceHelper.GetConfluencePageContent(contentval.id).then(function(response) {
                    res.say(confluenceHelper.formatConfluencePageContent(response)).send();
                }).catch(function(err) {
                    var prompt;
                    if (err.statusCode === 404) {
                        console.log(err.message);
                        prompt = 'Sorry The requested page was not found';
                    } else {
                        prompt = 'Sorry , I am not able to reach the API server';
                    }
                    res.say(prompt).send();
                });
            }
        }
    }
    return false;
});

//hack to support custom utterances in utterance expansion string
var utterancesMethod = app.utterances;
app.utterances = function() {
    return utterancesMethod().replace(/\{\-\|/g, '{');
};

module.exports = app;
